import 'reflect-metadata';

import * as bodyParser from 'body-parser';
import * as morgan from "morgan";

import { InversifyExpressServer } from 'inversify-express-utils';

import './controller';

import { LogStatus, PORT_NUMBER } from './constant';

import {
    LoggerService,
    LoggerServiceImplementation,
    CONTAINER
} from './service';

let server = new InversifyExpressServer(CONTAINER);

server.setConfig((app) => {
    app.use(bodyParser.urlencoded({
        extended: true
    }));
    app.use(bodyParser.json());
    app.use(morgan('dev'));
});

let logger: LoggerService = new LoggerServiceImplementation();
let serverInstance = server.build();

serverInstance.listen(PORT_NUMBER, () => {
    logger.log(`App is running at http://localhost:${PORT_NUMBER}`,
        LogStatus.INFO);
    logger.log('Press CTRL+C to stop\n', LogStatus.INFO);
});
