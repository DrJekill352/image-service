import * as mongoose from 'mongoose';

export interface Image {
    image: string;
    name: string;
    description: string;
}

export interface ServerImage extends mongoose.Document, Image {
    image: string;
    name: string;
    description: string;
}

export const IMAGE_SCHEMA = {
    image: {
        type: String,
        required: true
    },
    name: {
        type: String,
        unique: true,
        required: true
    },
    description: {
        type: String,
        required: true
    }
};
