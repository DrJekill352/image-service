import {
  controller,
  httpGet,
  httpPost,
  httpPut
} from 'inversify-express-utils';

import { Request, Response } from 'express';
import { inject } from 'inversify';

import { ImageRepository } from '../service';
import { Image } from '../models';

@controller('/')
export class ImageController {
  constructor(@inject(ImageRepository) private imageRepository: ImageRepository) { }

  @httpGet('/:name')
  public getImage(request: Request): Promise<Image> {
    return this.imageRepository.getImage(request.params.name);
  }

  @httpPost('/')
  public saveImage(request: Request, response: Response): Promise<Response> {
    return this.imageRepository.saveImage(request.body.image, request.body.name, request.body.description)
      .then(() => response.sendStatus(200))
      .catch(() => response.sendStatus(500));
  }

  @httpPut('/:name')
  public updateImage(request: Request, response: Response): Promise<Response> {
    return this.imageRepository.updateImage(request.params.name,
      { image: request.body.image, name: request.body.name, description: request.body.description } as Image)
      .then(() => response.sendStatus(200))
      .catch(() => response.sendStatus(500));
  }
}
