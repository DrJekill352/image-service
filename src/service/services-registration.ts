import { Container } from 'inversify';
import { LoggerService, LoggerServiceImplementation } from './logger';
import { ImageRepository, ImageRepositoryImplementation } from './db';

export const CONTAINER = new Container();

CONTAINER.bind<LoggerService>(LoggerService).to(LoggerServiceImplementation);
CONTAINER.bind<ImageRepository>(ImageRepository).to(ImageRepositoryImplementation);
