import { Image } from "../../../models";

export abstract class ImageRepository {
    public abstract saveImage(image: string, name: string, description: string): Promise<Image>;
    public abstract getImage(name: string): Promise<Image>;
    public abstract updateImage(name: string, image: Image): Promise<Image>;
}
