import { injectable, inject } from "inversify";

import {
    Mongoose,
    Schema,
    Model,
} from "mongoose";

import { ImageRepository } from "./image.repository";
import { LoggerService } from "../../logger";

import {
    LogStatus,
    DB_URL,
    NAME_IMAGE_COLLECTION
} from "../../../constant";

import {
    ServerImage,
    IMAGE_SCHEMA,
    Image
} from "../../../models";

@injectable()
export class ImageRepositoryImplementation implements ImageRepository {
    private mongoose: Mongoose = new Mongoose();
    private imageModel: Model<ServerImage> = this.mongoose.model<ServerImage>(NAME_IMAGE_COLLECTION, new Schema(IMAGE_SCHEMA));

    constructor(
        @inject(LoggerService) private loggerService: LoggerService
    ) {
        this.mongoose.connect(DB_URL);
    }

    public saveImage(image: string, name: string, description: string): Promise<Image> {
        const newImage: ServerImage = new this.imageModel({ image: image, name: name, description: description } as ServerImage);

        return new Promise<ServerImage>((resolve, reject) => {
            newImage.save((error, data: ServerImage) => {
                if (error) {
                    reject(error);
                    this.loggerService.log(error.errmsg, LogStatus.ERROR);
                } else {
                    resolve(data);
                    this.loggerService.log(`Save ${data.name} image success`, LogStatus.INFO);
                }
            });
        });
    }

    public getImage(name: string): Promise<Image> {
        return new Promise<ServerImage>((resolve, reject) => {
            this.imageModel.findOne({ name: name }, (error, data: ServerImage) => {
                if (error) {
                    reject(error);
                    this.loggerService.log(error.message, LogStatus.ERROR);
                } else {
                    if (!data) {
                        reject(new Error('Image not found'));
                    } else {
                        resolve(data);
                        this.loggerService.log(`Get ${data.name} image success`, LogStatus.INFO);
                    }
                }
            });
        });
    }

    public updateImage(name: string, image: Image): Promise<Image> {
        return new Promise<ServerImage>((resolve, reject) => {
            this.imageModel.findOneAndUpdate({ name: name }, image, (error, data: ServerImage) => {
                if (error) {
                    reject(error);
                    this.loggerService.log(error.message, LogStatus.ERROR);
                } else {
                    if (!data) {
                        reject(new Error('Image not found'));
                    } else {
                        resolve(data);
                        this.loggerService.log(`Update ${data.name} image success`, LogStatus.INFO);
                    }
                }
            });
        });

    }
}
