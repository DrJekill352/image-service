import 'reflect-metadata';

import { Request, Response } from "express";

import { ImageController } from "../../controller";
import { ImageRepository } from "../../service";
import { ServerImage } from "../../models";

class MockResponse {
    public sendStatus(code: number): any {
        return { status: code };
    }
}

class ImageRepositoryMock implements ImageRepository {
    public saveImage(image: string, name: string, description: string): Promise<ServerImage> {
        return new Promise<ServerImage>((resolve, reject) => {
            if (image && name && describe) {
                resolve({
                    image: image,
                    name: name,
                    description: description
                } as ServerImage);
            } else {
                reject(new Error('Save Fail'));
            }
        });
    }

    public getImage(id: string): Promise<ServerImage> {
        return new Promise<ServerImage>((resolve, reject) => {
            resolve({
                image: 'image',
                name: 'name',
                description: 'asdfdafdafsafasfsfaafsafaf'
            } as ServerImage);
        });
    }

    public updateImage(id: string, image: ServerImage): Promise<ServerImage> {
        return new Promise<ServerImage>((resolve, reject) => {
            if (id && image) {
                resolve({
                    image: 'image',
                    name: 'name',
                    description: 'asdfdafdafsafasfsfaafsafaf'
                } as ServerImage);
            } else {
                reject(new Error('Update Fail'));
            }

        });
    }
}

const constImage: ServerImage = {
    image: 'image',
    name: 'name',
    description: 'asdfdafdafsafasfsfaafsafaf'
} as ServerImage;

describe('ImageController', () => {
    let controller: ImageController;



    beforeEach(() => {
        controller = new ImageController(new ImageRepositoryMock());
    });

    it('should return ok status on save image', async () => {
        const response = await controller.saveImage({
            body: constImage
        } as Request, new MockResponse() as Response);
        expect(response.status).toEqual(200);
    });

    it('should return error status on save image', async () => {
        const response = await controller.saveImage({
            body: {}
        } as Request, new MockResponse() as Response);
        expect(response.status).toEqual(500);
    });

    it('should return image', async () => {
        const response = await controller.getImage({ params: { id: 1 } } as Request);
        expect(response).toEqual(constImage);
    });

    it('should update image', async () => {
        const response = await controller.updateImage({
            params: {
                name: 'name'
            },
            body: constImage
        } as Request, new MockResponse() as Response);
        expect(response.status).toEqual(200);
    });

    it('should return error on update image', async () => {
        const response = await controller.updateImage({
            params: {
                id: undefined
            },
            body: constImage
        } as Request, new MockResponse() as Response);
        expect(response.status).toEqual(500);
    });
});
