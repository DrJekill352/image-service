import 'reflect-metadata';

import {
    ImageRepository,
    ImageRepositoryImplementation,
    LoggerServiceImplementation
} from '../../service';

import { ServerImage, Image } from '../../models';

describe('ImageRepositoryImplementation', () => {
    let service: ImageRepository;
    let imageName: string;

    const image: ServerImage = {
        image: String(Math.random()).substring(2),
        name: String(Math.random()).substring(2),
        description: 'image description'
    } as ServerImage;

    beforeEach(() => {
        service = new ImageRepositoryImplementation(new LoggerServiceImplementation());
    });

    it('should save Image', async () => {
        const response: Image = await service.saveImage(image.image, image.name, image.description);
        expect(response.image).toEqual(image.image);
        imageName = response.name;
    });

    it('should reject save Image', async (done: DoneFn) => {
        try {
            const response = await service.saveImage(image.image, image.name, image.description);
        } catch (error) {
            expect(error.errmsg).toContain('duplicate key error collection');
            done();
        }
    });

    it('should get Image', async () => {
        const response = await service.getImage(imageName);
        expect(response.image).toEqual(image.image);
    });

    it('should reject not found get Image', async (done: DoneFn) => {
        try {
            const response = await service.getImage('abc');
        } catch (error) {
            expect(error.message).toContain('Image not found');
            done();
        }
    });

    it('should update Image', async () => {
        const newImage: string = String(Math.random()).substring(2);

        const response = await service.updateImage(imageName, { image: newImage, name: image.name, description: image.description } as ServerImage);

        expect(response).not.toBeUndefined();
    });

    it('should reject not found update Image', async (done: DoneFn) => {
        try {
            const response = await service.updateImage('abc', {} as ServerImage);
        } catch (error) {
            expect(error.message).toContain('Image not found');
            done();
        }
    });
});
